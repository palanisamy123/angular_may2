import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map,catchError } from 'rxjs/operators';
import { Company } from './Company';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonsrvService {

  private url = 'assets/sample_data.json';
  private apiurl = 'http://localhost:53888//Api/Company';
  constructor(private http: HttpClient) {

   }

   getSampleData()
   {
     return this.http.get(this.url)
     .pipe(map((response: Response) => response),catchError(this.errorHandler));
   }

   postCompanyData(company: Company)
   {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.post<Company>(this.apiurl + '/PostCompany/', company, httpOptions)
     .pipe(
       map((response: any) => response),catchError(this.errorHandler)
     );
   }

   errorHandler(error: Response) {  
    console.log(error);  
    return Observable.throw(error);  
}  
}

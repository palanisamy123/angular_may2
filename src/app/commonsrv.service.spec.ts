import { TestBed } from '@angular/core/testing';

import { CommonsrvService } from './commonsrv.service';

describe('CommonsrvService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommonsrvService = TestBed.get(CommonsrvService);
    expect(service).toBeTruthy();
  });
});

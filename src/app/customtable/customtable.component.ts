import { Component, OnInit, Input, ChangeDetectorRef, ApplicationRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Time } from '@angular/common';
import { CommonsrvService } from '../commonsrv.service';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Company } from '../Company';





@Component({
  selector: 'customtable',
  templateUrl: './customtable.component.html',
  styleUrls: ['./customtable.component.css']
})
export class CustomtableComponent implements OnInit {
sampleData = [];
heads = ['Name', 'Phone','Email','Company','EntryDate','OrgNum','Address','City','Zip','Geo','Pan','Pin','Id','Status','Fee',
'Guid','ExitDate','DateFirst','DateRecent','Url'
];
p: number = 1;
public company:Company = new Company();
private pageSize: number = 25;
  constructor(private service: CommonsrvService,private http: HttpClient) {
  }

  ngOnInit() {
    this.service.getSampleData()
    .subscribe((res : any) => this.sampleData = res);
  }

  onSubmit(selectedItem: any)
  {
    this.company.Id = selectedItem.id;
    this.company.Status = selectedItem.status;
    
    this.service.postCompanyData(this.company).subscribe(
      () => {
            console.log('Id:' + this.company.Id, 'Status:' + this.company.Status);
      }
    );
  }

}
